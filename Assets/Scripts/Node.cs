﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node{

    public Point2D p;
    public float c;
    public float gScore;
    public float fScore;

	public Node (Point2D p, float c)
    {
        this.p = p;
        this.c = c;
        gScore = float.MaxValue;
        fScore = float.MaxValue;
    }
}
