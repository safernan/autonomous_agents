﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dialogs : MonoBehaviour {

    public Color color;
    public string agentName;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void say(string sentence)
    {
        this.GetComponent<GUIText>().text = agentName+ ": "+sentence;
        this.GetComponent<GUIText>().color = color;
    }
}
