﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobState : State<Jesse> {

    static readonly RobState instance = new RobState();

    public static RobState Instance
    {
        get
        {
            return instance;
        }
    }

    static RobState() { }
    private RobState() { }

    public override void Enter(Jesse agent)
    {
        agent.findPathTo(Tiles.Bank);
    }

    public override void Execute(Jesse agent)
    {
        agent.senseWyatt();
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() == Tiles.Bank)
        {
            agent.robTheBank();
            agent.ChangeState(LurkState.Instance);
        }
    }

    public override void Exit(Jesse agent)
    {
    }
}
