﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LurkState : State<Jesse> {

    static readonly LurkState instance = new LurkState();

    public static LurkState Instance
    {
        get
        {
            return instance;
        }
    }

    static LurkState() { }
    private LurkState() { }

    public override void Enter(Jesse agent)
    {
        agent.findPathTo(Tiles.OutlawCamp);
        agent.resetTime();
    }

    public override void Execute(Jesse agent)
    {
        agent.senseWyatt();
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        int loc = agent.rnd.Next(0, 10);
        //if not in camp or cemetary, or if decide to move
        if (loc == 0 || (agent.getLocation() != Tiles.OutlawCamp && agent.getLocation() != Tiles.Cemetery))
        {
            if (agent.getLocation() == Tiles.OutlawCamp)
                agent.findPathTo(Tiles.Cemetery);
            else
                agent.findPathTo(Tiles.OutlawCamp);
            return;
        }
        int rob = agent.rnd.Next(0, 100);
        if (rob < 5) agent.ChangeState(RobState.Instance);
    }

    public override void Exit(Jesse agent)
    {
    }
}
