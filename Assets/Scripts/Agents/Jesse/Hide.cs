﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideState : State<Jesse> {

    static readonly HideState instance = new HideState();

    public static HideState Instance
    {
        get
        {
            return instance;
        }
    }

    static HideState() { }
    private HideState() { }

    public override void Enter(Jesse agent)
    {
    }

    public override void Execute(Jesse agent)
    {
        if (agent.hiddenLongEnough())
        {
            agent.ChangeState(LurkState.Instance);
        }
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        agent.hidingTime++;
    }

    public override void Exit(Jesse agent)
    {
    }
}
