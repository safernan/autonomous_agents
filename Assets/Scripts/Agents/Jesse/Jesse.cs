﻿using UnityEngine;
using System.Collections.Generic;

public class Jesse : Agent
{

    public static Jesse instance = null;

    private StateMachine<Jesse> stateMachine;

    public static int SPEED_MULT = 15;
    public static int HIDING_TIME = 100;


    public int hidingTime = 0;
    public int gold = 0;


    public delegate void bankRobbery();
    public static event bankRobbery OnBankRobbery;

    public delegate void killing();
    public static event killing OnKilling;
    

    public void Awake()
    {
        id = 2;
        speedMult = SPEED_MULT;
        path = new LinkedList<Point2D>();
        this.stateMachine = new StateMachine<Jesse>();
        this.stateMachine.Init(this, LurkState.Instance);
        Wyatt.OnOutlawKilling += respawn;
        Wyatt.OnKilledByOutlaw += killSheriff;
        instance = this;
        sense = new Sense();
        sense.OnSawAgent += localize;
        sense.OnHeardAgent += localize;
    }
    public void localize(Agent agent)
    {
        if (agent == Wyatt.instance)
        {
            Vector2 w = TilingSystem.instance.Location2Grid(agent.transform.position.x, agent.transform.position.y);
            Vector2 camp = TilingSystem.instance.getGridOf(Tiles.OutlawCamp);
            Vector2 j = TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y);
            if (System.Math.Abs(j.x - camp.x) + System.Math.Abs(j.y - camp.y) < System.Math.Abs(w.x - camp.x) + System.Math.Abs(w.y - camp.y)) 
            {
                findPathTo((int)camp.x, (int)camp.y);
                say("Damned! I am repered!");
                ChangeState(HideState.Instance);
            }
        }
    }
    public bool hiddenLongEnough()
    {
        return (hidingTime > HIDING_TIME);
    }
    public void senseWyatt()
    {
        sense.pos = this.transform.position;
        sense.lookAround(sight);
        sense.listenTo(Wyatt.instance, hearing);
    }
    public void resetTime()
    {
        hidingTime = 0;
    }
    public void respawn()
    {
        say("AAARGHH!!");
        gold = 0;
        TeleportToPlace(Tiles.OutlawCamp);
        path = new LinkedList<Point2D>();
        ChangeState(LurkState.Instance);
    }
    public void Start()
    {
        //world = new global::Grid();
        TeleportToPlace(Tiles.OutlawCamp);
    }
    
    public void robTheBank()
    {
        this.gold += rnd.Next(0, 20);
        say("I have now "+gold+"$" );
        if (OnBankRobbery != null)
            OnBankRobbery();
    }
    public void killSheriff()
    {
        say("There is a corpse here. \n I go somewhere else :3");
        if (OnKilling != null)
            OnKilling();
    }

    public void ChangeState(State<Jesse> state)
    {
        this.stateMachine.ChangeState(state);
    }

    public override void Update()
    {
        this.stateMachine.Update();
    }
}
