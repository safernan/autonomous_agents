﻿using UnityEngine;
using System.Collections.Generic;

public class Bob : Agent
{

    public static Bob instance = null;
    private StateMachine<Bob> stateMachine;

    public static int SALOON_TIME = 200;
    public static int MINE_TIME = 400;
    public static int FATIGUE_MAX = 2000;

    public static int SPEED_MULT = 18;

    int mineTime = 0;
    int saloonTime = 0;
    int fatigue = 0;

    public void Awake()
    {
        id = 0;
        speedMult = SPEED_MULT;
        path = new LinkedList<Point2D>();
        this.stateMachine = new StateMachine<Bob>();
        this.stateMachine.Init(this, MineState.Instance);
        Jesse.OnBankRobbery += cryForMoney;
        Wyatt.OnBobGreetings += sayHello;
        Elsa.OnBobDrunk += goHome;
        Elsa.OnCallBob += hearElsaCall;
        sense = new Sense();
        sense.OnHeardAgent += goHome;
        instance = this;
    }
    public void hearElsaCall()
    {
        sense.listenTo(Elsa.instance, hearing);
    }
    public void goHome()
    {
        findPathTo(Tiles.House);
    }
    public void goHome(Agent a)
    {
        if (a == Elsa.instance)
        {
            findPathTo(Tiles.House);
            say("I am coming !");
        }
    }
    public void sayHello() { say("Hello!"); }
    public void cryForMoney()
    {
        say("My money has been stolen!");
    }
    public void Start()
    {
        TeleportToPlace(Tiles.Mine);
    }
    public void IncreaseMineTime(int amount)
    {
        this.mineTime += amount;
    }
    public void IncreaseFatigue(int amount)
    {
        this.fatigue += amount;
    }

    public void IncreaseSaloonTime(int amount)
    {
        this.saloonTime += amount;
    }

    public bool MinedLongEnough()
    {
        return this.mineTime >= MINE_TIME;
    }
    public bool DrankLongEnough()
    {
        return this.saloonTime >= SALOON_TIME;
    }
    public bool RestedEnough()
    {
        return this.fatigue < 1;
    }
    public bool TooTired()
    {
        return this.fatigue >= FATIGUE_MAX;
    }

    public void ResetTime()
    {
        this.mineTime = 0;
        this.saloonTime = 0;
    }
    
    public void ChangeState(State<Bob> state)
    {
        this.stateMachine.ChangeState(state);
    }

    public void goToPlace(Tiles t)
    {
        Vector2 pos = getLocationOf(t);
        transform.position = new Vector3(pos.x, pos.y, 0);
    }

    public override void Update()
    {
        this.stateMachine.Update();
    }
}
