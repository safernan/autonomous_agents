﻿using UnityEngine;

public sealed class RestState : State<Bob> {
	
	static readonly RestState instance = new RestState();
	
	public static RestState Instance {
		get {
			return instance;
		}
	}
	
	static RestState() {}
	private RestState() {}
	
	public override void Enter (Bob agent) {
		agent.say("I need to sleep...");
        agent.findPathTo(Tiles.House);
    }
	
	public override void Execute (Bob agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.House)
        {
            agent.findPathTo(Tiles.House);
            return;
        }
		agent.IncreaseFatigue(-5);
        if (agent.RestedEnough())  agent.ChangeState(MineState.Instance);
	}
	
	public override void Exit (Bob agent) {
        agent.ResetTime();
	}
}
