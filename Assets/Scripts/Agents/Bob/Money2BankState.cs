﻿using UnityEngine;

public sealed class Money2BankState : State<Bob> {
	
	static readonly Money2BankState instance = new Money2BankState();
	
	public static Money2BankState Instance {
		get {
			return instance;
		}
	}
	
	static Money2BankState() {}
	private Money2BankState() {}
	
	public override void Enter (Bob agent) {
		agent.say("I have a lot of money for the bank !");
        agent.findPathTo(Tiles.Bank);
    }
	
	public override void Execute (Bob agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Bank)
        {
            agent.findPathTo(Tiles.Bank);
            return;
        }
        agent.ChangeState(SaloonState.Instance);
	}
	
	public override void Exit (Bob agent) {
	}
}
