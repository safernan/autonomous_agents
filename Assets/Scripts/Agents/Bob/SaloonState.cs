using UnityEngine;

public sealed class SaloonState : State<Bob> {

	static readonly SaloonState instance = new SaloonState();

	public static SaloonState Instance {
		get {
			return instance;
		}
	}
    private float initialHearing;
	static SaloonState() {}
	private SaloonState() {}

	public override void Enter (Bob agent) {
		agent.say("I go to the saloon...");
        agent.findPathTo(Tiles.Saloon);
        initialHearing = agent.hearing;
    }

	public override void Execute (Bob agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Saloon)
        {
            agent.findPathTo(Tiles.Saloon);
            return;
        }
        agent.IncreaseSaloonTime(1);
        agent.IncreaseFatigue(1);
        if (agent.hearing > 0)
            agent.hearing -= 1;
        if (agent.TooTired()) agent.ChangeState(RestState.Instance);
        else
		if (agent.DrankLongEnough()) agent.ChangeState(MineState.Instance);
	}

	public override void Exit (Bob agent) {
		agent.say("I am drunk!");
        agent.hearing = initialHearing;
        agent.ResetTime();
	}
}
