﻿using UnityEngine;

public sealed class MineState : State<Bob> {
	
	static readonly MineState instance = new MineState();
	
	public static MineState Instance {
		get {
			return instance;
		}
	}
	
	static MineState() {}
	private MineState() {}
	
	public override void Enter (Bob agent) {
		agent.say("I go to the mine...");
        agent.findPathTo(Tiles.Mine);
    }
	
	public override void Execute (Bob agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Mine)
        {
            agent.findPathTo(Tiles.Mine);
            return;
        }
		agent.IncreaseMineTime(1);
        agent.IncreaseFatigue(2);
        if (agent.TooTired()) agent.ChangeState(RestState.Instance);
        else
        if (agent.MinedLongEnough())  agent.ChangeState(Money2BankState.Instance);
	}
	
	public override void Exit (Bob agent) {
        agent.ResetTime();
	}
}
