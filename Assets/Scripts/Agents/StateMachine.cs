﻿public class StateMachine <T> {
	
	private T agent;
    private State<T> state;
    private State<T> previousState;
    private State<T> Globalstate;

    public void Awake () {
		this.state = null;
	}

	public void Init (T agent, State<T> startState) {
		this.agent = agent;
		this.state = startState;
	}

	public void Update () {
        if (this.Globalstate != null) this.Globalstate.Execute(this.agent);
		if (this.state != null) this.state.Execute(this.agent);
	}
	
	public void ChangeState (State<T> nextState) {
        this.previousState = this.state;
		if (this.state != null) this.state.Exit(this.agent);
		this.state = nextState;
		if (this.state != null) this.state.Enter(this.agent);
	}
    public void RevertToPreviousState()
    {
        ChangeState(previousState);
    }
}