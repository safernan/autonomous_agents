﻿using UnityEngine;

public sealed class OfficeState : State<Wyatt> {
	
	static readonly OfficeState instance = new OfficeState();
	
	public static OfficeState Instance {
		get {
			return instance;
		}
	}
	
	static OfficeState() {}
	private OfficeState() {}
	
	public override void Enter (Wyatt agent) {
        agent.findPathTo(Tiles.SheriffsOffice);
    }
	
	public override void Execute (Wyatt agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.SheriffsOffice)
        {
            agent.findPathTo(Tiles.SheriffsOffice);
            return;
        }
        agent.IncreaseOfficeTime(1);
        if (agent.OfficeLongEnough())
		    agent.ChangeState(PatrolState.Instance);
	}
	
	public override void Exit (Wyatt agent) {
        agent.ResetTime();
	}
}
