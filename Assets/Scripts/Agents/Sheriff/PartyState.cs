using UnityEngine;

public sealed class PartyState : State<Wyatt> {

	static readonly PartyState instance = new PartyState();

	public static PartyState Instance {
		get {
			return instance;
		}
	}

	static PartyState() {}
	private PartyState() {}

	public override void Enter (Wyatt agent) {
        agent.say("Let's go to the saloon to celebrate how I am good!");
        agent.findPathTo(Tiles.Saloon);
    }

	public override void Execute (Wyatt agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Saloon)
        {
            agent.findPathTo(Tiles.Saloon);
            return;
        }
        agent.IncreaseSaloonTime(1);
		if (agent.DrankLongEnough()) agent.ChangeState(OfficeState.Instance);
	}

	public override void Exit (Wyatt agent) {
        agent.ResetTime();
	}
}
