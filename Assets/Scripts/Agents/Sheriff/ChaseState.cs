﻿using UnityEngine;

public sealed class ChaseState : State<Wyatt> {
	
	static readonly ChaseState instance = new ChaseState();
	
	public static ChaseState Instance {
		get {
			return instance;
		}
	}
	
	static ChaseState() {}
	private ChaseState() {}
	
	public override void Enter (Wyatt agent) {
        agent.speedState = 0.75f;
    }
	
	public override void Execute (Wyatt agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            if (agent.getLocation() != Tiles.OutlawCamp)
            {
                agent.senseJesse();
                agent.lookForJesse();
            }
            return;
        }
        agent.ChangeState(PatrolState.Instance);
	}
	
	public override void Exit (Wyatt agent) {
        agent.speedState = 1f;
        agent.ResetTime();
	}
}
