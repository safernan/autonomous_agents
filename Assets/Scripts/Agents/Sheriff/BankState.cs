using UnityEngine;

public sealed class BankState : State<Wyatt> {

	static readonly BankState instance = new BankState();

	public static BankState Instance {
		get {
			return instance;
		}
	}

	static BankState() {}
	private BankState() {}

	public override void Enter (Wyatt agent) {
        agent.findPathTo(Tiles.Bank);
    }

	public override void Execute (Wyatt agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Bank)
        {
            agent.findPathTo(Tiles.Bank);
            return;
        }
        agent.say("This money belongs to the bank !\n Here is your money :)");
		agent.ChangeState(PartyState.Instance);
	}

	public override void Exit (Wyatt agent) {
        agent.ResetTime();
	}
}
