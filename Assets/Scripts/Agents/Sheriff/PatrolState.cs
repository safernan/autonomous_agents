﻿using UnityEngine;

public sealed class PatrolState : State<Wyatt> {
	
	static readonly PatrolState instance = new PatrolState();
	
	public static PatrolState Instance {
		get {
			return instance;
		}
	}
	
	static PatrolState() {}
	private PatrolState() {}
	
	public override void Enter (Wyatt agent) {
		agent.say("I start a new patrol. \n There still exists outlaws !");
        agent.speedState = 1;
    }
	
	public override void Execute (Wyatt agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            if (agent.getLocation() != Tiles.OutlawCamp)
            {
                agent.senseJesse();
                agent.lookForJesse();
            }
            agent.IncreasePatrolTime(1);
            return;
        }
        if (agent.PatrolledLongEnough())
            agent.ChangeState(OfficeState.Instance);
        else
            agent.findPathTo(agent.rnd.Next(0, (int) TilingSystem.instance.MapSize.x-1), agent.rnd.Next(0, (int) TilingSystem.instance.MapSize.y-1));
	}
	
	public override void Exit (Wyatt agent) {
        agent.ResetTime();
	}
}
