﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Wyatt : Agent
{
    public static Wyatt instance = null;

    private StateMachine<Wyatt> stateMachine;

    public static int SALOON_TIME = 250;
    public static int PATROL_TIME = 6000;
    public static int OFFICE_TIME = 200;
    public static int SPEED_MULT = 14;

    public int patrolTime = 0;
    public int saloonTime = 0;
    public int officeTime = 0;


    public delegate void outlawKilling();
    public static event outlawKilling OnOutlawKilling;

    public delegate void killedByOutlaw();
    public static event killedByOutlaw OnKilledByOutlaw;

    public delegate void BobGreetings();
    public static event BobGreetings OnBobGreetings;

    public delegate void ElsaGreetings();
    public static event ElsaGreetings OnElsaGreetings;

    public delegate void NytroGreetings();
    public static event NytroGreetings OnNytroGreetings;


    public void Awake()
    {
        id = 4;
        speedMult = SPEED_MULT;
        path = new LinkedList<Point2D>();
        this.stateMachine = new StateMachine<Wyatt>();
        this.stateMachine.Init(this, OfficeState.Instance);
        instance = this;
        sense = new Sense();
        sense.OnSawAgent += localize;
        sense.OnHeardAgent += localize;
    }

    public void Start()
    {
        TeleportToPlace(Tiles.SheriffsOffice);
    }
    public void IncreasePatrolTime(int amount)
    {
        this.patrolTime += amount;
    }

    public void IncreaseSaloonTime(int amount)
    {
        this.saloonTime += amount;
    }
    public void IncreaseOfficeTime(int amount)
    {
        this.officeTime += amount;
    }

    public bool PatrolledLongEnough()
    {
        return this.patrolTime >= PATROL_TIME;
    }
    public bool DrankLongEnough()
    {
        return this.saloonTime >= SALOON_TIME;
    }
    public bool OfficeLongEnough()
    {
        return this.officeTime >= OFFICE_TIME;
    }

    public void ResetTime()
    {
        this.patrolTime = 0;
        this.saloonTime = 0;
        this.officeTime = 0;
    }

    public void ChangeState(State<Wyatt> state)
    {
        this.stateMachine.ChangeState(state);
    }

    public void goToPlace(Tiles t)
    {
        Vector2 pos = getLocationOf(t);
        transform.position = new Vector3(pos.x, pos.y, 0);
    }

    public override void Update()
    {
        this.stateMachine.Update();
    }
    private void localize(Agent agent)
    {
        if (agent == Jesse.instance)
        {
            Vector2 j = TilingSystem.instance.Location2Grid(agent.transform.position.x, agent.transform.position.y);
            Vector2 camp = TilingSystem.instance.getGridOf(Tiles.OutlawCamp);
            if ( Math.Abs(camp.x - (int)j.x) + Math.Abs(camp.y - (int)j.y) < 1)
            {
                ChangeState(PatrolState.Instance);
                return;
            }
            say("Jesse!");
            findPathTo((int)j.x, (int)j.y);
            ChangeState(ChaseState.Instance);
        }
    }

    public void senseJesse()
    {
        sense.pos = this.transform.position;
        sense.lookAround(sight);
        sense.listenTo(Jesse.instance, hearing);
    }
    public void lookForJesse()
    {
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
            TilingSystem.instance.Location2Grid(Jesse.instance.transform.position.x, Jesse.instance.transform.position.y))
        {
            int die = rnd.Next(0,2);
            if (die == 1)//die
            {
                if (OnKilledByOutlaw != null)
                    OnKilledByOutlaw();
                respawn();
                return;
            }
            if (OnOutlawKilling != null)
                OnOutlawKilling();
            say("*PAN!* There is a corpse. \n I take that money and go to the bank.");
            ChangeState(BankState.Instance);
        }
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
           TilingSystem.instance.Location2Grid(Bob.instance.transform.position.x, Bob.instance.transform.position.y))
        {
            if (OnBobGreetings != null)
                OnBobGreetings();
            say("Hello Bob!");
        }
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
           TilingSystem.instance.Location2Grid(Elsa.instance.transform.position.x, Elsa.instance.transform.position.y))
        {
            if (OnElsaGreetings != null)
                OnElsaGreetings();
            say("Hello dear Lady!");
        }
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
           TilingSystem.instance.Location2Grid(Nytroglycerin.instance.transform.position.x, Nytroglycerin.instance.transform.position.y))
        {
            if (OnNytroGreetings != null)
                OnNytroGreetings();
            say("Hello, undertaker!");
        }
        return;
    }
    public void respawn()
    {
        say("Tell my wife I love her. Arg.");
        TeleportToPlace(Tiles.SheriffsOffice);
        path = new LinkedList<Point2D>();
        ResetTime();
        ChangeState(OfficeState.Instance);
    }

}
