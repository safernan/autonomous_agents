﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverState : State<Nytroglycerin> {

    static readonly HoverState instance = new HoverState();

    public static HoverState Instance
    {
        get
        {
            return instance;
        }
    }

    static HoverState() { }
    private HoverState() { }

    public override void Enter(Nytroglycerin agent)
    {
        agent.say("Let's wait for new clients !\n Hihihi !");
        agent.findPathTo(Tiles.Undertakers);
    }

    public override void Execute(Nytroglycerin agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Undertakers)
        {
            agent.findPathTo(Tiles.Undertakers);
            return;
        }
        if (agent.bodies.Count > 0)
            agent.ChangeState(TakeBodyState.Instance);
    }

    public override void Exit(Nytroglycerin agent)
    {
    }
}
