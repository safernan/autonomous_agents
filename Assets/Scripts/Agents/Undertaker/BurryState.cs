﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurryState : State<Nytroglycerin> {

    static readonly BurryState instance = new BurryState();

    public static BurryState Instance
    {
        get
        {
            return instance;
        }
    }

    static BurryState() { }
    private BurryState() { }

    public override void Enter(Nytroglycerin agent)
    {
        //slow down because of body
        agent.speedState = 2;
        agent.findPathTo(Tiles.Cemetery);
    }

    public override void Execute(Nytroglycerin agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Cemetery)
        {
            agent.findPathTo(Tiles.Cemetery);
            return;
        }
        if (agent.bodies.Count > 0)
            agent.ChangeState(TakeBodyState.Instance);
        else
            agent.ChangeState(HoverState.Instance);
    }

    public override void Exit(Nytroglycerin agent)
    {
        agent.speedState = 1;
    }
}
