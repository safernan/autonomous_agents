﻿using UnityEngine;
using System.Collections.Generic;

public class Nytroglycerin : Agent
{

    public static Nytroglycerin instance = null;

    private StateMachine<Nytroglycerin> stateMachine;

    public int SPEED_MULT = 20;

    public LinkedList<Vector2> bodies;

    

    public void Awake()
    {
        id = 3;
        speedMult = SPEED_MULT;
        path = new LinkedList<Point2D>();
        bodies = new LinkedList<Vector2>();
        this.stateMachine = new StateMachine<Nytroglycerin>();
        this.stateMachine.Init(this, HoverState.Instance);
        instance = this;
        Wyatt.OnOutlawKilling += takeOutlawCorpse;
        Wyatt.OnNytroGreetings += sayHello;
        Jesse.OnKilling += takeCorpseFromOutlaw;
    }
    public void sayHello()
    {
        say("Lovely day to die, isn't it?");
    }
    public void Start()
    {
        TeleportToPlace(Tiles.Undertakers);
    }

    public void takeBody()
    {
        say("Oh, he is heavy!");
        TilingSystem.instance.RemoveBody();
    }
    

    public void ChangeState(State<Nytroglycerin> state)
    {
        this.stateMachine.ChangeState(state);
    }

    public override void Update()
    {
        this.stateMachine.Update();
    }
    public void takeCorpseFromOutlaw()
    {
        this.bodies.AddLast(TilingSystem.instance.Location2Grid(Jesse.instance.transform.position.x, Jesse.instance.transform.position.y));
        TilingSystem.instance.AddBodyToMap(Jesse.instance.transform.position);
    }
    public void takeOutlawCorpse()
    {
        this.bodies.AddLast(TilingSystem.instance.Location2Grid(Wyatt.instance.transform.position.x, Wyatt.instance.transform.position.y));
        TilingSystem.instance.AddBodyToMap(Wyatt.instance.transform.position);
    }
}
