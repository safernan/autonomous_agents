﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeBodyState : State<Nytroglycerin> {

    static readonly TakeBodyState instance = new TakeBodyState();

    public static TakeBodyState Instance
    {
        get
        {
            return instance;
        }
    }

    static TakeBodyState() { }
    private TakeBodyState() { }

    public override void Enter(Nytroglycerin agent)
    {
        agent.say("A new client is waiting for me");
        agent.findPathTo( (int)agent.bodies.First.Value.x, (int)agent.bodies.First.Value.y);
        agent.bodies.RemoveFirst();
    }

    public override void Execute(Nytroglycerin agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        agent.takeBody();
        agent.ChangeState(BurryState.Instance);
    }

    public override void Exit(Nytroglycerin agent)
    {
    }
}
