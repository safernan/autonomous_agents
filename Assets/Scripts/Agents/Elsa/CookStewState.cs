using UnityEngine;

public sealed class CookStewState : State<Elsa> {

	static readonly CookStewState instance = new CookStewState();

	public static CookStewState Instance {
		get {
			return instance;
		}
	}

	static CookStewState() {}
	private CookStewState() {}

	public override void Enter (Elsa agent) {
        agent.say("I feel like cooking a nice stew for Bob!");
        agent.findPathTo(Tiles.House);
	}

	public override void Execute (Elsa agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.House)
        {
            agent.findPathTo(Tiles.House);
            return;
        }
        agent.IncreaseCookingTime(1);
        if (agent.CookedLongEnough()) agent.ChangeState(WaitForBobState.Instance);
    }

	public override void Exit (Elsa agent) {
        agent.ResetTime();
	}
}
