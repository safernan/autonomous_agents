﻿using UnityEngine;
using System.Collections.Generic;

public class Elsa : Agent
{

    public static Elsa instance = null;
    private StateMachine<Elsa> stateMachine;

    public static int SPEED_MULT = 16;
    public static int STEW_COOKING_TIME = 300;
    public static int DRINKING_TIME = 400;
    public static int WAITING_TIME = 300;
    public static int WALKING_TIME = 1000;

    int cookingTime = 0;
    int walkingTime = 0;
    int drinkingTime = 0;
    int waitingTime = 0;

    public delegate void findBobDrunk();
    public static event findBobDrunk OnBobDrunk;
    public delegate void call4Bob();
    public static event call4Bob OnCallBob;

    public void Awake()
    {
        id = 1;
        speedMult = SPEED_MULT;
        path = new LinkedList<Point2D>();
        this.stateMachine = new StateMachine<Elsa>();
        this.stateMachine.Init(this, CookStewState.Instance);
        Wyatt.OnElsaGreetings += sayHello;
        instance = this;
        sense = new Sense();
        sense.OnHeardAgent += runHome;
        sense.OnSawAgent += runHome;
    }
    public void Start()
    {
        TeleportToPlace(Tiles.House);
    }
    public void sayHello() { say("Hello Mister Wyatt!"); }

    public void IncreaseCookingTime(int amount)
    {
        this.cookingTime += amount;
    }
    public void IncreaseWalkingTime(int amount)
    {
        this.walkingTime += amount;
    }
    public bool CookedLongEnough()
    {
        return this.cookingTime > STEW_COOKING_TIME;
    }
    public bool walkedEnough()
    {
        return this.walkingTime > WALKING_TIME;
    }
    public void IncreaseDrinkingTime(int amount)
    {
        this.drinkingTime += amount;
    }
    public bool DrankEnough()
    {
        return this.drinkingTime > DRINKING_TIME;
    }
    public void stopWait()
    {
        this.waitingTime = 0;
    }
    public void IncreaseWaitingTime(int amount)
    {
        this.waitingTime += amount;
    }
    public bool WaitedEnough()
    {
        return this.waitingTime > WAITING_TIME;
    }
    public void runHome(Agent a)
    {
        if (a == Jesse.instance)
        {
            say("IIH! An outlaw!");
            findPathTo(Tiles.House);
        }
    }

    public void ChangeState(State<Elsa> state)
    {
        this.stateMachine.ChangeState(state);
    }

    public override void Update()
    {
        this.stateMachine.Update();
    }
    public void senseJesse()
    {
        sense.lookAround(sight);
        sense.listenTo(Jesse.instance, hearing);
    }
    public void lookForBobDrunk()
    {
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
           TilingSystem.instance.Location2Grid(Bob.instance.transform.position.x, Bob.instance.transform.position.y))
        {
            if (OnBobDrunk != null)
                OnBobDrunk();
            say("You here!? ALCOOLIC! \n GRRRR!! \n Go home, now, the stew is ready!");
        }
    }
    public bool lookForBobStew()
    {
        if (TilingSystem.instance.Location2Grid(this.transform.position.x, this.transform.position.y) ==
           TilingSystem.instance.Location2Grid(Bob.instance.transform.position.x, Bob.instance.transform.position.y))
        {
            ChangeState(WalkState.Instance);
            return true;
        }
        return false;
    }

    public void callBob()
    {
        if (OnCallBob != null)
            OnCallBob();
        say("Bob!! I have made a lovely stew for youuu!");
       
    }

    public void ResetTime()
    {
        this.cookingTime = 0;
        this.drinkingTime = 0;
        this.waitingTime = 0;
        this.walkingTime = 0;
    }
}
