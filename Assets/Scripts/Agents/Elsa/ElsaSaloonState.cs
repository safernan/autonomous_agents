using UnityEngine;

public sealed class ElsaSaloonState : State<Elsa> {

	static readonly ElsaSaloonState instance = new ElsaSaloonState();

	public static ElsaSaloonState Instance {
		get {
			return instance;
		}
	}

	static ElsaSaloonState() {}
	private ElsaSaloonState() {}

	public override void Enter (Elsa agent) {
        agent.findPathTo(Tiles.Saloon);
	}

	public override void Execute (Elsa agent)
    {
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
            return;
        }
        if (agent.getLocation() != Tiles.Saloon)
        {
            agent.findPathTo(Tiles.Saloon);
            return;
        }
        agent.lookForBobDrunk();
        agent.IncreaseDrinkingTime(1);
        if (agent.DrankEnough()) agent.ChangeState(WalkState.Instance);
    }

	public override void Exit (Elsa agent) {
        agent.ResetTime();
	}
}
