﻿using UnityEngine;

public sealed class WalkState : State<Elsa> {
	
	static readonly WalkState instance = new WalkState();
	
	public static WalkState Instance {
		get {
			return instance;
		}
	}
	
	static WalkState() {}
	private WalkState() {}
	
	public override void Enter (Elsa agent) {
		agent.say("I'll go for a walk!");
        agent.findPathTo(agent.rnd.Next(0, (int)TilingSystem.instance.MapSize.x - 1), agent.rnd.Next(0, (int)TilingSystem.instance.MapSize.y - 1));
    }

    public override void Execute (Elsa agent)
    {
        agent.IncreaseWalkingTime(1);
        if (agent.path.Count > 0)
        {
            agent.makeNextStep();
        }
        agent.senseJesse();
        if (agent.walkedEnough() && !TilingSystem.instance.isDay())
            agent.ChangeState(CookStewState.Instance);
        else
        if (agent.path.Count == 0)
        {
            agent.IncreaseWaitingTime(1);
            if (agent.WaitedEnough() && TilingSystem.instance.isDay())
            {
                agent.stopWait();
                agent.findPathTo(agent.rnd.Next(0, (int)TilingSystem.instance.MapSize.x - 1), agent.rnd.Next(0, (int)TilingSystem.instance.MapSize.y - 1));
            }
        }
    }
	
	public override void Exit (Elsa agent) {
        agent.ResetTime();
	}
}
