using UnityEngine;

public sealed class WaitForBobState : State<Elsa> {

	static readonly WaitForBobState instance = new WaitForBobState();

	public static WaitForBobState Instance {
		get {
			return instance;
		}
	}

	static WaitForBobState() {}
	private WaitForBobState() {}

	public override void Enter (Elsa agent) {
        agent.callBob();
	}

	public override void Execute (Elsa agent)
    {
        if(agent.lookForBobStew())
            return;
        agent.IncreaseWaitingTime(1);
        if (agent.WaitedEnough())
        {
            agent.say("Where is he ?");
            agent.ChangeState(ElsaSaloonState.Instance);
        }
    }

	public override void Exit (Elsa agent) {
        agent.ResetTime();
	}
}
