﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

abstract public class Agent : MonoBehaviour {

    public static bool SHOW_PATH = true;
    public LinkedList<Point2D> path;
    public System.Random rnd = new System.Random();
    public float speedTile = 1; //speed from terrain
    public float speedState = 1; //speed from state (drunk, carrying heavy...)
    public float speedMult = 20; //intrinsic speed
    public float sight = 8.0f;
    public float hearing = 8.0f;
    public int last_step = 0;
    public int id;
    public Sense sense = new Sense();

    public Dialogs dialog;

    abstract public void Update ();
    public Tiles getLocation() {
        Vector3 pos = transform.position;
        return TilingSystem.instance.getLocationType((int)pos.x, (int) pos.y);
    }
    public Vector2 getLocationOf(Tiles t)
    {
        return TilingSystem.instance.getLocationOf(t);
    }

    public void findPathTo(Tiles t)
    {
        Vector2 to = TilingSystem.instance.getGridOf(t);
        Vector2 from = TilingSystem.instance.Location2Grid(transform.position.x, transform.position.y);
        path = TilingSystem.instance.map.Solve((int)from.x, (int)from.y, (int)to.x, (int)to.y);
        if (path.Count > 0)
            path.RemoveFirst();
        if (SHOW_PATH)
        {
            TilingSystem.instance.clearPath(id);
            TilingSystem.instance.AddPathToMap(new List<Point2D>(path), id);
        }
    }
    public void findPathTo(int x, int y)
    {
        Vector2 from = TilingSystem.instance.Location2Grid(transform.position.x, transform.position.y);
        path = TilingSystem.instance.map.Solve((int)from.x, (int)from.y, x, y);
        if (path.Count > 0)
            path.RemoveFirst();
        if (SHOW_PATH)
        {
            TilingSystem.instance.clearPath(id);
            TilingSystem.instance.AddPathToMap(new List<Point2D>(path), id);
        }
    }
    public void TeleportToPlace(Tiles t)
    {
        Vector2 pos = getLocationOf(t);
        transform.position = new Vector3(pos.x, pos.y, 0);
        if (SHOW_PATH)
            TilingSystem.instance.clearPath(id);
    }
    public void say(string message)
    {
        dialog.say(message);
    }

    public void makeNextStep()
    {
        if (last_step < speedTile * speedState * speedMult)
        {
            last_step++;
            return;
        }
        else
            last_step = 0;
        if (path.Count == 0)
            return;
        Tiles here = TilingSystem.instance.getGridType(path.First.Value.x, path.First.Value.y);
        speedTile = 1;
        if (here == Tiles.Mountain)
            speedTile = 3;
        if (here == Tiles.Water)
            speedTile = 8;
        Vector2 p = TilingSystem.instance.Grid2Location(path.First.Value.x, path.First.Value.y);
        path.RemoveFirst();
        transform.position = new Vector3(p.x, p.y, 0);
        if (SHOW_PATH)
            TilingSystem.instance.RemovePath(id);
    }
}