﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sense : MonoBehaviour {

    public delegate void SeeAgent(Agent agent);
    public event SeeAgent OnSawAgent;

    public delegate void HearAgent(Agent agent);
    public event HearAgent OnHeardAgent;
    public Vector3 pos;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void lookAround(float sight)
    {
        float tresh = sight;
        if (!TilingSystem.instance.isDay())
            tresh/=2;
        for (int i = -1; i < 2; ++i)
            for (int j = -1; j < 2; ++j)
                if (j != 0 || i != 0)
                {
                    RaycastHit2D[] hits = Physics2D.RaycastAll(pos, new Vector2(i, j).normalized);
                    for (int c = 0; c < hits.Length; ++c)
                    {
                        RaycastHit2D hit = hits[c];
                        {
                            if (hit.collider.GetComponent<SpriteRenderer>() != null)
                                if (hit.collider.GetComponent<SpriteRenderer>().sprite != null)
                                    if (hit.collider.GetComponent<SpriteRenderer>().sprite.name != null)
                                    {
                                        if (hit.distance > tresh)
                                            continue;
                                        var name = hit.collider.GetComponent<SpriteRenderer>().sprite.name;
                                        if (name == "mountain"
                                            || name == "saloon"
                                            || name == "undertakers"
                                            || name == "sheriffoffice"
                                            || name == "mine"
                                            || name == "house"
                                            || name == "bank")
                                            break;
                                        if (name == "jesse")
                                            if (OnSawAgent != null)
                                                OnSawAgent(Jesse.instance);
                                        if (name == "wyatt")
                                            if (OnSawAgent != null)
                                                OnSawAgent(Wyatt.instance);
                                        if (name == "elsa")
                                            if (OnSawAgent != null)
                                                OnSawAgent(Elsa.instance);
                                        if (name == "bob")
                                            if (OnSawAgent != null)
                                                OnSawAgent(Bob.instance);
                                        if (name == "undertaker")
                                            if (OnSawAgent != null)
                                                OnSawAgent(Nytroglycerin.instance);
                                    }
                        }
                    }
                }
    }
    public void listenTo(Agent a, float hearing)
    {
        Vector2 to = TilingSystem.instance.Location2Grid(a.transform.position.x, a.transform.position.y);
        Vector2 from = TilingSystem.instance.Location2Grid(pos.x, pos.y);
        float dist = TilingSystem.instance.map.Hear((int)from.x, (int)from.y, (int)to.x, (int)to.y);
        if (dist < hearing)
            if (OnHeardAgent != null)
                OnHeardAgent(a);
    }
}
