﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid {

    public int maxX;
    public int maxY;
    public Node[,] grid;
    public LinkedList<Point2D> open;
    public LinkedList<Point2D> closed;
    public Point2D[,] cameFrom;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public Grid()
    {
        refreshHuman();
    }
    //cost initilisation for moving
    public void refreshHuman()
    {
        TilingSystem ts = TilingSystem.instance;
        maxX = (int)ts.MapSize.x;
        maxY = (int)ts.MapSize.y;
        grid = new Node[maxX, maxY];
        cameFrom = new Point2D[maxX, maxY];
        for (int x = 0; x < maxX; ++x)
            for (int y = 0; y < maxY; ++y)
            {
                float cost = 1f;
                if (ts.getGridType(x, y) == Tiles.Mountain)
                    cost = 3f;
                if (ts.getGridType(x, y) == Tiles.Water)
                    cost = 8f;
                grid[x, y] = new global::Node(new Point2D(x, y), cost);
                cameFrom[x, y] = null;
            }
        open = new LinkedList<Point2D>();
        closed = new LinkedList<Point2D>();
    }
    //cost initilisation for hearing
    public void refreshSound()
    {
        TilingSystem ts = TilingSystem.instance;
        maxX = (int)ts.MapSize.x;
        maxY = (int)ts.MapSize.y;
        grid = new Node[maxX, maxY];
        cameFrom = new Point2D[maxX, maxY];
        for (int x = 0; x < maxX; ++x)
            for (int y = 0; y < maxY; ++y)
            {
                float cost = 1f;
                //not so bad propagation
                if (ts.getGridType(x, y) == Tiles.Bank
                    || ts.getGridType(x, y) == Tiles.SheriffsOffice
                    || ts.getGridType(x, y) == Tiles.House
                    || ts.getGridType(x, y) == Tiles.Bridge
                    || ts.getGridType(x, y) == Tiles.Undertakers)
                    cost = 3f;
                //bad propagation
                if (ts.getGridType(x, y) == Tiles.Water
                    || ts.getGridType(x, y) == Tiles.Saloon
                    || ts.getGridType(x, y) == Tiles.Mine
                    || ts.getGridType(x, y) == Tiles.Mountain)
                    cost = 8f;
                grid[x, y] = new global::Node(new Point2D(x, y), cost);
                cameFrom[x, y] = null;
            }
        open = new LinkedList<Point2D>();
        closed = new LinkedList<Point2D>();
    }
    //manhattan distance
    public float costEstimate(int x1, int y1, int x2, int y2)
    {
        return 1f*( Mathf.Abs(x2 - x1) + Mathf.Abs(y2 - y1));
    }
    public LinkedList<Point2D> reconstruct_path(Point2D[,] cameFrom, Point2D current)
    {
        LinkedList<Point2D> totalPath = new LinkedList<Point2D>();
        totalPath.AddFirst(current);
        Point2D orig = cameFrom[current.x, current.y];
        while (orig != null)
        {
            current = orig;
            orig = cameFrom[current.x, current.y];
            totalPath.AddFirst(current);
        }
        return (totalPath);
    }
    public void addInOpen(Node n)
    {
        Point2D after = null;
        foreach(Point2D p in open)
        {
            if (grid[p.x, p.y].fScore > n.fScore)
                break;
            after = p;
        }
        if (after == null)
            open.AddFirst(n.p);
        else
            open.AddAfter(open.Find(after), n.p);
    }
    public LinkedList<Point2D> Solve(int x1, int y1, int x2, int y2)
    {
        //init
        refreshHuman();
        grid[x1, y1].gScore = 0;
        grid[x1, y1].fScore = costEstimate(x1, y1, x2, y2);
        open.AddFirst(grid[x1, y1].p);
        int mult = 1;
        //loop
        while (open.Count > 0)
        {
            mult *= -1;
            Point2D current = open.First.Value;
            if (current.x == x2 && current.y == y2)
                return reconstruct_path(cameFrom, current);
            open.RemoveFirst();
            closed.AddFirst(current);
            for (int i = -1; i < 2; i ++)
                for (int j = -1; j < 2; j++)
                    if ((i == 0 && j!=0)||(i!=0 && j == 0))
                    {
                        int x = current.x + mult*i;
                        int y = current.y + mult*j;
                        if (x < 0 || y < 0 || x >= maxX || y >= maxY)
                            continue;
                        Node neighbor = grid[x, y];
                        float tentative_gScore = grid[current.x, current.y].gScore + neighbor.c;
                        if (open.Contains(neighbor.p) && tentative_gScore < neighbor.gScore)
                            open.Remove(neighbor.p);
                        if (closed.Contains(neighbor.p) && tentative_gScore < neighbor.gScore)
                            closed.Remove(neighbor.p);
                        if ((!open.Contains(neighbor.p)) && (!closed.Contains(neighbor.p)))
                        {
                            grid[x, y].gScore = tentative_gScore;
                            grid[x, y].fScore = tentative_gScore + costEstimate(neighbor.p.x, neighbor.p.y, x2, y2);
                            addInOpen(grid[x, y]);
                            cameFrom[x, y] = current;
                        }
                    }
        }
        return null;
    }
    public float Hear(int x1, int y1, int x2, int y2)
    {
        //init
        refreshSound();
        grid[x1, y1].gScore = 0;
        grid[x1, y1].fScore = costEstimate(x1, y1, x2, y2);
        open.AddFirst(grid[x1, y1].p);
        int mult = 1;
        //loop
        while (open.Count > 0)
        {
            mult *= -1;
            Point2D current = open.First.Value;
            if (current.x == x2 && current.y == y2)
            {
                if (cameFrom[current.x, current.y] != null)
                    return grid[cameFrom[current.x, current.y].x, cameFrom[current.x, current.y].y].gScore + grid[current.x, current.y].c;
                else return grid[current.x, current.y].c;
            }
            open.RemoveFirst();
            closed.AddFirst(current);
            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                    if ((i == 0 && j != 0) || (i != 0 && j == 0))
                    {
                        int x = current.x + mult * i;
                        int y = current.y + mult * j;
                        if (x < 0 || y < 0 || x >= maxX || y >= maxY)
                            continue;
                        Node neighbor = grid[x, y];
                        float tentative_gScore = grid[current.x, current.y].gScore + neighbor.c;
                        if (open.Contains(neighbor.p) && tentative_gScore < neighbor.gScore)
                            open.Remove(neighbor.p);
                        if (closed.Contains(neighbor.p) && tentative_gScore < neighbor.gScore)
                            closed.Remove(neighbor.p);
                        if ((!open.Contains(neighbor.p)) && (!closed.Contains(neighbor.p)))
                        {
                            grid[x, y].gScore = tentative_gScore;
                            grid[x, y].fScore = tentative_gScore + costEstimate(neighbor.p.x, neighbor.p.y, x2, y2);
                            addInOpen(grid[x, y]);
                            cameFrom[x, y] = current;
                        }
                    }
        }
        return 100;
    }

}
