﻿using UnityEngine;
using System.Collections;

public enum Tiles {

    // add the location types here
    // ...	
    Unset,
    OutlawCamp,
    SheriffsOffice,
    Undertakers,
    Cemetery,
    Grass,
    Saloon,
    Mine,
    Bank,
    Mountain,
    House,
    Water,
    Bridge
}
