using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Lean; //from Unity asset "LeanPool" - freely available in the Asset Store; used here for object pooling

public class TilingSystem : MonoBehaviour {

	public List<TileSprite> TileSprites;
    public Sprite body;
    public List<Sprite> path;
    public Vector2 MapSize;
	public Sprite DefaultImage;
	public GameObject TileContainerPrefab;
	public GameObject TilePrefab;
    public GameObject BodiesContainerPrefab;
    public GameObject PathContainerPrefab;
	public Vector2 ViewPortSize;
    public Grid map;
    public Camera cam;
    public GameObject Sun;
    public GameObject Moon;

    public static TilingSystem instance = null;

    private Vector2[] _specialLoc;
    private TileSprite[,] _map;
    private GameObject _tileContainer;
    private GameObject _bodyContainer;
    private GameObject _pathContainer;
    private List<GameObject> _tiles = new List<GameObject>();
    private LinkedList<GameObject> _bodies = new LinkedList<GameObject>();
    private LinkedList<GameObject>[] _paths;
    public Vector2 CurrentPosition;
    private float hour = 80.0f;

    void Awake()
    {

            instance = this;
        cam.orthographicSize = MapSize.x;
        CurrentPosition = new Vector2((int)(MapSize.x/2.0f - 0.5f), (int)(MapSize.y/2.0f - 0.5f));
        _map = new TileSprite[(int)MapSize.x, (int)MapSize.y];
        _specialLoc = new Vector2[11];
        DefaultTiles();
        SetTiles();
        AddTilesToMap();
        map = new Grid();
        _paths = new LinkedList<GameObject>[5];
        for (int i = 0; i < 5; ++i)
            _paths[i] = new LinkedList<GameObject>();
        Moon.SetActive(false);

    }

    //create a map of size MapSize of unset tiles
    private void DefaultTiles()
    {
        for (var y = 0; y < MapSize.y ; y++)
        {
            for (var x = 0; x < MapSize.x; x++)
            {
                _map[x, y] = new TileSprite("unset", DefaultImage, Tiles.Unset);
            }
        }

    }

	//set the tiles of the map to what is specified in TileSprites
	private void SetTiles() {

        //grass
        for (var y = 0; y < MapSize.y; y++)
        {
            for (var x = 0; x < MapSize.x; x++)
            {

                _map[x, y] = new TileSprite(TileSprites[0].Name, TileSprites[0].TileImage, TileSprites[0].Tiletype);
            }
        }
        System.Random rnd = new System.Random();
        //add Moutains 
        for (int i = 0; i < MapSize.x*MapSize.y/5; ++i)
        {
            int x = rnd.Next(0, (int)MapSize.x);
            int y = rnd.Next(0, (int)MapSize.y);
            _map[x, y] = new TileSprite(TileSprites[8].Name, TileSprites[8].TileImage, TileSprites[8].Tiletype);
        }
        //add a river
        int yr = rnd.Next(1, (int)MapSize.y-1);
        for (int i = 0; i < (int)MapSize.y; ++i)
            _map[i, yr] = new TileSprite(TileSprites[10].Name, TileSprites[10].TileImage, TileSprites[10].Tiletype);
        //bridge
        int n_bridges = rnd.Next(1, 4);
        for (int b = 0; b < n_bridges; ++b)
        {
            int xr = rnd.Next(0, (int)(MapSize.y/ n_bridges))+(int)((b*(MapSize.y))/n_bridges);
            _map[xr, yr] = new TileSprite(TileSprites[11].Name, TileSprites[11].TileImage, TileSprites[11].Tiletype);
            _map[xr, yr - 1] = new TileSprite(TileSprites[0].Name, TileSprites[0].TileImage, TileSprites[0].Tiletype);
            _map[xr, yr + 1] = new TileSprite(TileSprites[0].Name, TileSprites[0].TileImage, TileSprites[0].Tiletype);
        }
        //add special location
        for (int i = 1; i < TileSprites.Count; ++i)
        {
            if (TileSprites[i].Tiletype == Tiles.Mountain || TileSprites[i].Tiletype == Tiles.Water || TileSprites[i].Tiletype == Tiles.Bridge)
                continue;
            bool fail = true;
            int x = 0;
            int y = 0;
            while (fail)
            {
                x = rnd.Next(0, (int)MapSize.x);
                y = rnd.Next(0, (int)MapSize.y);
                if (_map[x, y].Tiletype == Tiles.Grass 
                    || _map[x, y].Tiletype == Tiles.Mountain 
                    )
                    fail = false;
            }
            _map[x, y] = new TileSprite(TileSprites[i].Name, TileSprites[i].TileImage, TileSprites[i].Tiletype);
            _specialLoc[(int)(TileSprites[i].Tiletype)] = new Vector2(x, y);
        }
    }

	private void AddTilesToMap() {

        _tileContainer = LeanPool.Spawn(TileContainerPrefab);
        _bodyContainer = LeanPool.Spawn(BodiesContainerPrefab);
        _pathContainer = LeanPool.Spawn(PathContainerPrefab);

        var tileSize = 2f;
		int viewOffsetX = (int)ViewPortSize.x;
		int viewOffsetY = (int)ViewPortSize.y;

		for (int y = -viewOffsetY; y < viewOffsetY; y++) {
			for (int x = -viewOffsetX; x < viewOffsetX; x++) {
				var tX = x * tileSize;
				var tY = y * tileSize;

				var iX = x + (int)CurrentPosition.x;
				var iY = y + (int)CurrentPosition.y;

				if (iX < 0)
					continue;
				if (iY < 0)
					continue;
				if (iX > MapSize.x-1)
					continue;
				if (iY > MapSize.y-1)
					continue;

				var tile = LeanPool.Spawn (TilePrefab);
				tile.transform.position = new Vector3 (tX, tY, 0);
				tile.transform.SetParent (_tileContainer.transform);
				

				var renderer = tile.GetComponent<SpriteRenderer> ();
				renderer.sprite = _map [(int)x + (int)CurrentPosition.x, (int)y + (int)CurrentPosition.y].TileImage;
				
				_tiles.Add (tile);
			}
        }
        Sun.transform.position = _tiles[(int)((MapSize.x) * (MapSize.y) - 1)].transform.position + new Vector3(2 * tileSize, 0, 0);
        Moon.transform.position = _tiles[(int)((MapSize.x) * (MapSize.y) - 1)].transform.position + new Vector3(2 * tileSize, 0, 0);
    }

    public void AddBodyToMap(Vector3 pos)
    {
        var tile = LeanPool.Spawn(TilePrefab);
        tile.transform.position = pos;
        tile.transform.SetParent(_bodyContainer.transform);


        var renderer = tile.GetComponent<SpriteRenderer>();
        renderer.sprite = body;
        _bodies.AddLast(tile);
    }
    public void AddPathToMap(List<Point2D> _path, int _id)
    {
        LinkedList < GameObject > AgentPath = new LinkedList<GameObject>();
        for (int i = 0; i < _path.Count; ++i)
        {
            Vector2 p = Grid2Location(_path[i].x, _path[i].y);
            var tile = LeanPool.Spawn(TilePrefab);
            tile.transform.position = new Vector3(p.x, p.y, 0);
            tile.transform.SetParent(_pathContainer.transform);


            var renderer = tile.GetComponent<SpriteRenderer>();
            renderer.sprite = path[_id];
            AgentPath.AddLast(tile);
        }
        _paths[_id] = AgentPath;
    }
    public void RemoveBody()
    {
        var tile = _bodies.First.Value;
        LeanPool.Despawn(tile);
        _bodies.RemoveFirst();
    }
    public void RemovePath(int _id)
    {
        var tile = _paths[_id].First.Value;
        LeanPool.Despawn(tile);
        _paths[_id].RemoveFirst();
    }
    public void clearPath(int _id)
    {
        while (_paths[_id].Count > 0)
        {
            var tile = _paths[_id].First.Value;
            LeanPool.Despawn(tile);
            _paths[_id].RemoveFirst();
        }
    }


    public void Start()
    {
    }


    public Vector2 Location2Grid(float x, float y)
    {
        var tileSize = 2f;
        float i = ((x + MapSize.x) / tileSize - 0.5f);
        float j = ((y + MapSize.y) / tileSize - 0.5f);
        return new Vector2(i, j);
    }
    public Vector2 Grid2Location(int x, int y)
    {
        var tileSize = 2f;
        return new Vector2(((float)x + 1) * tileSize - 2*(int)((MapSize.x + 1) / 2), ((float)y + 1f) * tileSize - 2*(int)((MapSize.y + 1) / 2));
    }
    public Vector2 getLocationOf(Tiles t)
    {
        return Grid2Location((int)_specialLoc[(int)t].x, (int)_specialLoc[(int)t].y);
    }
    public Vector2 getGridOf(Tiles t)
    {
        return _specialLoc[(int)t];
    }
    public Tiles getLocationType(float x, float y)
    {
        var tileSize = 2f;
        int i = (int)((x + MapSize.x) / tileSize - 0.5f);
        int j = (int)((y + MapSize.y) / tileSize - 0.5f);
        if (i < 0 || j < 0 || i > MapSize.x || j > MapSize.y )
            return Tiles.Unset;
        return getGridType(i,j);
    }
    public Tiles getGridType(int x, int y)
    {
        return _map[x, y].Tiletype;
    }

    private void setNight()
    {
        Sun.SetActive(false);
        Moon.SetActive(true);
    }
    private void setDay()
    {
        Sun.SetActive(true);
        Moon.SetActive(false);
    }
    public bool isDay()
    {
        return Sun.activeSelf;
    }

    public void Update()
    {
        hour += 0.1f;
        if (hour > 200.0f && hour < 200.2f)
            setNight();
        if (hour > 80.0f && hour < 80.2f)
            setDay();
        if (hour > 230.9)
            hour = 0.0f;
    }

}
