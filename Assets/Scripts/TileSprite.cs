﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Lean; //from Unity asset "LeanPool" - freely available in the Asset Store; used here for object pooling
[Serializable]
public class TileSprite
{
    public Sprite TileImage;
    public string Name;
    public Tiles Tiletype;
    public TileSprite()
    {
        Name = "Unset";
        TileImage = new Sprite();
        Tiletype = Tiles.Unset;
    }
    public TileSprite(string name, Sprite image, Tiles tile)
    {
        Name = name;
        this.TileImage = image;
        this.Tiletype = tile;
    }
}